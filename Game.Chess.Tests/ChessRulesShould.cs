using System;
using System.Collections.Generic;
using Game.Chess;
using Xunit;
using static Xunit.Assert;

namespace Tests
{
    public class ChessRulesShould
    {
        ChessRules m_Rules;

        public ChessRulesShould()
        {
            m_Rules = new ChessRules();
        }
        
        [Fact]
        public void EvaluateExceptionsShould()
        {
            Assert.Throws<ArgumentNullException>(() => ChessTurn.Load("Input_Bb4_mod_Invalid_Piece.json", string.Empty));
            Assert.Throws<ArgumentNullException>(() => ChessTurn.Load(string.Empty, Program.INPUT_DATA_FOLDER_NAME));
            Assert.Throws<ArgumentNullException>(() => ChessTurn.Load(string.Empty, string.Empty));
            
            Assert.Throws<ArgumentException>(() => m_Rules.EvaluateOptions(null));
        }
        
        public static IEnumerable<object[]> InvalidLayoutMockData =>
            new List<object[]>
            {
                new object[]
                {
                    ChessTurn.Load("Input_Bb4_mod_Invalid_Piece.json", Program.INPUT_DATA_FOLDER_NAME), "Error: Piece To Move does not appear upon the board.", ChessRules.Sides.Undefined
                },
                
                new object[]
                {
                    ChessTurn.Load("Input_Bb4_mod_Invalid_White_Piece.json", Program.INPUT_DATA_FOLDER_NAME), "Error: White zg3 is not formatted correctly.", ChessRules.Sides.Black
                }, 
                
                new object[]
                {
                    ChessTurn.Load("Input_Bb4_mod_Invalid_Black_Piece.json", Program.INPUT_DATA_FOLDER_NAME), "Error: Black zg3 is not formatted correctly.", ChessRules.Sides.Black
                },
            };

        [Theory]
        [MemberData(nameof(InvalidLayoutMockData))]
        public void EvaluateErrorsShould(ChessTurn turn, string validMoveAnswer, ChessRules.Sides side)
        {
            if (turn == null)
            {
                throw new ArgumentNullException(nameof(turn));
            }

            var validMoves = m_Rules.EvaluateOptions(turn);

            var concatMoves = string.Join(",", validMoves);

            Equal(turn.PieceToMoveSide, side);
            Equal(concatMoves.Trim(), validMoveAnswer);
        }

        public static IEnumerable<object[]> LayoutMockData =>
            new List<object[]>
            {
                new object[]
                {
                    ChessTurn.Load("Input_Kb8_mod_BishopThreatToKing.json", Program.INPUT_DATA_FOLDER_NAME), "LEGAL MOVES FOR Kb8: a8",
                    ChessRules.Sides.Black
                },
                new object[]
                {
                    ChessTurn.Load("Input_Bb4_mod_dupes.json", Program.INPUT_DATA_FOLDER_NAME), "LEGAL MOVES FOR Bb4: a3,a5,c5,d6,e7,f8,c3,d2,e1",
                    ChessRules.Sides.White
                }, // "a3,a5 enemy is vulnerable.,c5,d6,e7,f8,c3,d2,e1" White
                new object[]
                {
                    ChessTurn.Load("Input_Bb4_mod.json", Program.INPUT_DATA_FOLDER_NAME), "LEGAL MOVES FOR Bb4: a3,a5,c5,d6,e7,f8,c3,d2,e1",
                    ChessRules.Sides.White
                }, // "a3,a5 enemy is vulnerable.,c5,d6,e7,f8,c3,d2,e1" White
                new object[]
                {
                    ChessTurn.Load("Input_Qf5_mod.json", Program.INPUT_DATA_FOLDER_NAME),
                    "LEGAL MOVES FOR Qf5: e4,d3,c2,b1,e5,d5,c5,b5,a5,e6,d7,c8,f6,f7,f8,g6,h7,g5,h5,g4,h3,f4,f3",
                    ChessRules.Sides.White
                }, // e4,d3,c2,b1,e5,d5,c5,b5,a5 enemy is vulnerable.,e6,d7,c8,f6,f7,f8,g6,h7,g5,h5,g4,h3,f4,f3 White
                new object[] {ChessTurn.Load("Input_Kb8.json", Program.INPUT_DATA_FOLDER_NAME), "LEGAL MOVES FOR Kb8: a8,c8", ChessRules.Sides.Black}, // "a8,c8" Black
                new object[] {ChessTurn.Load("Input_Kb8.json", Program.INPUT_DATA_FOLDER_NAME), "LEGAL MOVES FOR Kb8: a8,c8", ChessRules.Sides.Black}, // "a8,c8" Black
                new object[] {ChessTurn.Load("Input_Kg1.json", Program.INPUT_DATA_FOLDER_NAME), "LEGAL MOVES FOR Kg1: g2,h1", ChessRules.Sides.White}, // "g2,h1" White
                new object[] {ChessTurn.Load("Input_Ne8.json", Program.INPUT_DATA_FOLDER_NAME), "LEGAL MOVES FOR Ne8: g7,f6,d6", ChessRules.Sides.Black}, // "g7,f7,d7" Black
                new object[] {ChessTurn.Load("Input_Pa7.json", Program.INPUT_DATA_FOLDER_NAME), "LEGAL MOVES FOR Pa7: a6", ChessRules.Sides.Black}, // "a6" Black
                new object[] {ChessTurn.Load("Input_Pb7.json", Program.INPUT_DATA_FOLDER_NAME), "LEGAL MOVES FOR Pb7: b6,b5", ChessRules.Sides.Black}, // "b6,b5" Black
                new object[] {ChessTurn.Load("Input_Pc7.json", Program.INPUT_DATA_FOLDER_NAME), "LEGAL MOVES FOR Pc7: c6,c5", ChessRules.Sides.Black}, // "c6,c5" Black
                new object[] {ChessTurn.Load("Input_Pf2.json", Program.INPUT_DATA_FOLDER_NAME), "LEGAL MOVES FOR Pf2: f3,f4", ChessRules.Sides.White}, // "f3,f4" White
                new object[] {ChessTurn.Load("Input_Pg3.json", Program.INPUT_DATA_FOLDER_NAME), "LEGAL MOVES FOR Pg3: g4", ChessRules.Sides.White}, // "g4" White
                new object[] {ChessTurn.Load("Input_Ph2.json", Program.INPUT_DATA_FOLDER_NAME), "LEGAL MOVES FOR Ph2: h3,h4", ChessRules.Sides.White}, // "h3,h4" White
                new object[] {ChessTurn.Load("Input_Nf5.json", Program.INPUT_DATA_FOLDER_NAME), "LEGAL MOVES FOR Nf5: d6,e7,h6,g7,h4,d4,e3", ChessRules.Sides.White}, // "h3,h4" White
                new object[]
                {
                    ChessTurn.Load("Input_Ra5.json", Program.INPUT_DATA_FOLDER_NAME), "LEGAL MOVES FOR Ra5: b5,c5,d5,e5,f5,g5,h5,a6,a4,a3,a2,a1", ChessRules.Sides.Black
                }, // "b5,c5,d5,e5,f5,g5,h5,a6,a4,a3,a2,a1" Black
                new object[]
                {
                    ChessTurn.Load("Input_Rf1.json", Program.INPUT_DATA_FOLDER_NAME), "LEGAL MOVES FOR Rf1: e1,d1,c1,b1,a1", ChessRules.Sides.White
                }, // "e1,d1,c1,b1,a1" White
            };

        [Theory]
        [MemberData(nameof(LayoutMockData))]
        public void EvaluateShould(ChessTurn turn, string validMoveAnswer, ChessRules.Sides side)
        {
            if(turn == null)
            {
                throw new ArgumentNullException(nameof(turn));
            }

            var validMoves = m_Rules.EvaluateOptions(turn);

            var concatMoves = string.Join(",", validMoves);

            Equal(turn.PieceToMoveSide, side);
            Equal(concatMoves, validMoveAnswer);
        }


        public static IEnumerable<object[]> DangerToKingMockData =>
            new List<object[]>
            {
                new object[]
                {
                    ChessTurn.Load("Input_Kb8_mod_BishopThreatToKing.json", Program.INPUT_DATA_FOLDER_NAME), new string[] { "a8", "c8" } , "c8" // , "Valid Move c8 excluded since king would be exposed to Nf5."
                }
            };

        [Theory]
        [MemberData(nameof(DangerToKingMockData))]
        public void EvaluateDangerToKingShould(ChessTurn turn, string[] attackingPositions, string validMoveAnswer)
        {
            if(turn == null)
            {
                throw new ArgumentNullException(nameof(turn));
            }

            var validMoves = m_Rules.EvaluateDangerToPosition(turn, attackingPositions);

            var concatMoves = string.Join(",", validMoves);
            
            Equal(concatMoves, validMoveAnswer);
        }

        [Theory]
        [InlineData("Input_Rf1.json")]
        [InlineData("Input_Rf1")]
        public void PlayShould(string input)
        {
            var msg = m_Rules.Play(input, Program.INPUT_DATA_FOLDER_NAME);
            False(string.IsNullOrEmpty(msg));
        }

        [Theory]
        [InlineData("Input_Rf1.json")]
        [InlineData("Input_Rf1")]
        public void LoadingCustomerConfigShould(string input)
        {
            ChessTurn turn = ChessTurn.Load(input, Program.INPUT_DATA_FOLDER_NAME);

            const string wActualString = "Rf1,Kg1,Pf2,Ph2,Pg3";
            string wTest = string.Join(",", turn.White);

            const string bActualString = "Kb8,Ne8,Pa7,Pb7,Pc7,Ra5";
            string bTest = string.Join(",", turn.Black);

#pragma warning disable CA1307 // Specify StringComparison
            Assert.Contains(wTest, wActualString);
            Assert.Contains(bTest, bActualString);
            Assert.Equal("Rf1", turn.PieceToMove);
#pragma warning restore CA1307 // Specify StringComparison
        }
    }
}