﻿using System;
using System.Collections.Generic;
using Game.Chess;
using Xunit;
using static Xunit.Assert;

namespace Tests
{
    public class ChessPositionShould
    {
        [Theory]
        [MemberData(nameof(PositionErrorMockData))]
        public void InValidNewPositionShould(ChessPosition pos)
        {
            False(pos?.IsValid());
        }

        [Fact]
        public void EvaluateExceptionsShould()
        {
            Assert.Throws<ArgumentException>(() => "".ToPosition(ChessRules.Sides.White));

            Assert.Throws<ArgumentException>(() =>
            {
                ChessPosition pos = new ChessPosition(string.Empty, ChessRules.Sides.Black);
            });


            Assert.Throws<ArgumentException>(() =>
            {
                ChessPosition pos = new ChessPosition("b3", ChessRules.Sides.Black);
            });

        }

        public static IEnumerable<object[]> PositionErrorMockData =>
            new List<object[]>
            {
                new object[] {"Rw1".ToPosition(ChessRules.Sides.White)},
                new object[] {"R21".ToPosition(ChessRules.Sides.White)},
                new object[] {"2f1".ToPosition(ChessRules.Sides.White)},
                new object[] {"Af1".ToPosition(ChessRules.Sides.White)},
                new object[] {"Rfa".ToPosition(ChessRules.Sides.White)}
            };

        [Theory]
        [MemberData(nameof(PositionValidMockData))]
        public void ValidNewPositionShould(ChessPosition pos)
        {
            if(pos == null)
            {
                throw new System.ArgumentNullException(nameof(pos));
            }

            True(pos.IsValid());
        }

        public static IEnumerable<object[]> PositionValidMockData =>
            new List<object[]>
            {
                new object[] {"Rf1".ToPosition(ChessRules.Sides.White)}
            };

        [Theory]
        [InlineData("Rf2", "Re3", 1)]
        [InlineData("Rf2", "Rd4", 2)]
        [InlineData("Rd8", "Rd8", 1)]
        // top edge of board, should return null.
        public void MoveLeftUpDiagonal(string start, string end, ushort tileCount)
        {
            var pStart = start.ToPosition(ChessRules.Sides.White);
            var pEnd = end?.ToPosition(ChessRules.Sides.White);

            var nLoc = ChessPosition.MoveLeftUpDiagonal(pStart, tileCount);

            Equal(nLoc, pEnd);
        }

        [Theory]
        [InlineData("Re4", "Rd3", 1)]
        [InlineData("Re3", "Rc1", 2)]
        [InlineData("Rc1", "Rc1", 1)]
        // bottom edge of board, should return null.
        public void MoveLeftDownDiagonal(string start, string end, ushort tileCount)
        {
            var pStart = start.ToPosition(ChessRules.Sides.White);
            var pEnd = end?.ToPosition(ChessRules.Sides.White);

            var nLoc = ChessPosition.MoveLeftDownDiagonal(pStart, tileCount);

            Equal(nLoc, pEnd);
        }

        [Theory]
        [InlineData("Rf2", "Rg3", 1)]
        [InlineData("Rf2", "Rh4", 2)]
        [InlineData("Rh4", "Rh4", 1)]
        // top edge of board, should return null.
        public void MoveRightUpDiagonal(string start, string end, ushort tileCount)
        {
            var pStart = start.ToPosition(ChessRules.Sides.White);
            var pEnd = end?.ToPosition(ChessRules.Sides.White);

            var nLoc = ChessPosition.MoveRightUpDiagonal(pStart, tileCount);

            Equal(nLoc, pEnd);
        }

        [Theory]
        [InlineData("Rf4", "Rg3", 1)]
        [InlineData("Rf4", "Rh2", 2)]
        [InlineData("Rh2", "Rh2", 1)]
        // bottom edge of board, should return null.
        public void MoveRightDownDiagonal(string start, string end, ushort tileCount)
        {
            var pStart = start.ToPosition(ChessRules.Sides.White);
            var pEnd = end?.ToPosition(ChessRules.Sides.White);

            var nLoc = ChessPosition.MoveRightDownDiagonal(pStart, tileCount);

            Equal(nLoc, pEnd);
        }

        [Theory]
        [InlineData("Re4", "Re5", 1)]
        [InlineData("Re4", "Re6", 2)]
        [InlineData("Re8", "Re8", 1)]
        // bottom edge of board, should return null.
        public void MoveUp(string start, string end, ushort tileCount)
        {
            var pStart = start.ToPosition(ChessRules.Sides.White);
            var pEnd = end?.ToPosition(ChessRules.Sides.White);

            var nLoc = ChessPosition.MoveUp(pStart, tileCount);

            Equal(nLoc, pEnd);
        }

        [Theory]
        [InlineData("Re4", "Re3", 1)]
        [InlineData("Re4", "Re2", 2)]
        [InlineData("Re1", "Re1", 1)]
        // bottom edge of board, should return null.
        public void MoveDown(string start, string end, ushort tileCount)
        {
            var pStart = start.ToPosition(ChessRules.Sides.White);
            var pEnd = end?.ToPosition(ChessRules.Sides.White);

            var nLoc = ChessPosition.MoveDown(pStart, tileCount);

            Equal(nLoc, pEnd);
        }

        [Theory]
        [InlineData("Re4", "Rf4", 1)]
        [InlineData("Re4", "Rg4", 2)]
        [InlineData("Rh4", "Rh4", 1)]
        // bottom edge of board, should return null.
        public void MoveRight(string start, string end, ushort tileCount)
        {
            var pStart = start.ToPosition(ChessRules.Sides.White);
            var pEnd = end?.ToPosition(ChessRules.Sides.White);

            var nLoc = ChessPosition.MoveRight(pStart, tileCount);

            Equal(nLoc, pEnd);
        }

        [Theory]
        [InlineData("Re4", "Rd4", 1)]
        [InlineData("Re4", "Rc4", 2)]
        [InlineData("Ra4", "Ra4", 1)]
        // bottom edge of board, should return null.
        public void MoveLeft(string start, string end, ushort tileCount)
        {
            var pStart = start.ToPosition(ChessRules.Sides.White);
            var pEnd = end?.ToPosition(ChessRules.Sides.White);

            var nLoc = ChessPosition.MoveLeft(pStart, tileCount);

            Equal(nLoc, pEnd);
        }


        [Fact]
        public void EqualsShould()
        {
            var p = "Rf1".ToPosition(ChessRules.Sides.White);
            var p2 = "rF1".ToPosition(ChessRules.Sides.White);

            Equal(p, p2);
        }
    }
}