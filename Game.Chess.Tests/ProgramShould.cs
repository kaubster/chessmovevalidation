using System;
using Game.Chess;
using Microsoft.VisualStudio.TestPlatform.TestHost;
using Xunit;
using static Xunit.Assert;

namespace Tests
{
    public class ProgramShould
    {
        [Theory]
        [InlineData("Input_Bb4_mod.json", "Input_Kb8.json", "Exit")]
        [InlineData("All", "Exit")]
        [InlineData("Exit")]
        public void MainWithArgs(params string[] input)
        {
            try { 
                Game.Chess.Program.MainEntry(input);
            }
#pragma warning disable CA1031 // Do not catch general exception types
            catch(Exception)
#pragma warning restore CA1031 // Do not catch general exception types
            {
                Assert.True(false);
            }
        }
    }
}