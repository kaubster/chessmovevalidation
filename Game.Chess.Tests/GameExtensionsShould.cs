﻿using System;
using Game.Chess;
using Xunit;
using static Xunit.Assert;

namespace Tests
{
    public class GameExtensionsShould
    {
        [Theory]
        [InlineData("f2", 5, 1)]
        public void ToLocation(string start, int colIndex, int rowIndex)
        {
            var loc = start.ToLocation();

            Equal(loc.ColumnIndex, colIndex);
            Equal(loc.RowIndex, rowIndex);
        }


        [Theory]
        [InlineData("")]
        public void ToLocationEmptyOrNull(string pos) => Throws<ArgumentException>(() => pos.ToLocation());

        [Theory]
        [InlineData("Rf2", 'R', 5, 1, ChessRules.Sides.White)]
        public void ToPosition(string start, char piece, int colIndex, int rowIndex, ChessRules.Sides side)
        {
            var loc = start.ToPosition(ChessRules.Sides.White);

            Equal(loc.Name, piece);
            Equal(loc.Side, side);
            Equal(loc.Location.ColumnIndex, colIndex);
            Equal(loc.Location.RowIndex, rowIndex);
        }

        [Theory]
        [InlineData("")]
        public void ToPositionEmptyOrNull(string pos)
        {
            Assert.Throws<ArgumentException>(() => pos.ToPosition(ChessRules.Sides.White));
        }
    }
}