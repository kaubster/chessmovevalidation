using System;
using Game;
using Xunit;
using static Xunit.Assert;

namespace Tests
{
    public class LocationShould
    {
        [Theory]
        [InlineData('a', '1', 0)]
        [InlineData('b', '2', 1)]
        [InlineData('c', '3', 2)]
        [InlineData('d', '4', 3)]
        [InlineData('e', '5', 4)]
        [InlineData('f', '6', 5)]
        [InlineData('g', '7', 6)]
        [InlineData('h', '8', 7)]
        public void IndexsBeZeroBased(char col, char row, int index)
        {
            var loc = new Location(col, row);

            Equal(loc.Column, col);
            Equal(loc.Row, row);

            Equal(loc.RowIndex, index);
            Equal(loc.ColumnIndex, index);
        }

        [Theory]
        [InlineData('@', '1')] // Invalid Row and Column... throws exception. Valid row, invalid colkumn
        [InlineData('a', '!')] // Invalid Row and Column... throws exception. Valid column, invalid row
        public void ConstrutordExceptions(char col, char row)
        {
            Throws<ArgumentException>(() => {
                Location loc = new Location(col, row);
            });
        }

        [Theory]
        [InlineData('z', '9')] // Invalid Row and Column... throws exception
        public void IndexsBeZeroBasedExceptions(char col, char row)
        {
            Location loc = new Location(col, row);

            Throws<ArgumentOutOfRangeException>(() => {
                int temp = loc.RowIndex;
            });

            Throws<ArgumentOutOfRangeException>(() => {
                int temp = loc.ColumnIndex;
            });
        }

        [Fact]
        public void ToLocationEmptyOrNull()
        {
            Throws<ArgumentException>(() =>
            {
                Location loc = new Location(string.Empty);
            });
        }
    }
}