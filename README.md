# ChessMoveValidation 

A move validation engine for the game of chess.

* Language: C#
* .NET Core Framework (SDK 2.1.505 x64)
* Unit Testing framework: XUnit
* Unit Testing Runner: Visual Studio and Resharper Test Explorers
* Static Analysis: Microsoft.CodeAnalysis.FxCopAnalyzers:
  * <https://github.com/dotnet/roslyn-analyzers>
  * <https://docs.microsoft.com/en-us/visualstudio/code-quality/use-roslyn-analyzers?view=vs-2019>
  * Warnings seen within Visual Studio IDE
* Code Coverage: JetBrains dotCover

Prerequisites:

* Visual Studio or .NET CLI: Building, Running, Publishing and Execution of Tests all requires installing .NET Core SDK 2.1 or higher.

## Usage Example

1. Open a command prompt
2. Browse to Game.Chess.exe

__Example Usage:__ 

Game.Chess.exe ./InputData/Input_Rf1.json

__expected output:__

LEGAL MOVES FOR Rf1: e1,d1,c1,b1,a1

## Build the Solution, using on of following options

* Visual Studio:

    1. Open Game.Chess.sln
    2. Menu: Build > Rebuild Solution OR press Ctrl+Shift+B

* .NET CLI:

    1. Open Developer Command Prompt
    2. cd .\Game.Chess
    3. dotnet msbuild -p:Configuration=Release

## Run the solution

* Visual Studio:

    1. Open Game.Chess.sln
    2. Menu: Debug > Start Debugging OR press F5

* .NET CLI:

    1. Open Developer Command Prompt
    2. cd .\Game.Chess
    3. dotnet run -c:Release

## Publish the Solution

The following will publish the cross platform .Net Core application as a win-x64 console application named Game.Chess.exe. File location will depend on which profile is selected:

Debug profile: see directory \Game.Chess\bin\Release\netcoreapp2.1\publish_Debug_x64\Game.Chess.exe

Release profile: see directory \Game.Chess\bin\Release\netcoreapp2.1\publish_Release_x64\Game.Chess.exe

* Visual Studio:

    1. Open Game.Chess.sln
    2. In Solution Explorer, Right Click "Game.Chess" then select Publish from the context menu. A "Publish" page will appear. 
    3. Select release profile: "Profile_Release"
    4. Click "Publish" button.

* .NET CLI:

    1. cd .\Game.Chess
    2. dotnet publish -r win-x64 -c release

Note: The configured Publish profile utilizes Deployment Mode self-contained. Therefore, its not necessary to have the .NET Core runtime installed to run this application. All dependencies required by the application are included within the win-x64\Publish folder. Double-click Game.Chess.exe.

* Running published "Framework Dependent" version of the application requires installing .NET Core Runtime 2.1 or higher.

* Running published "self-contained" version of the application does not require any previous framework installation.

## Execute Tests

* Visual Studio:

    1. Open Game.Chess.sln
    2. Menu: Test > Run > All Tests OR press Ctrl+R, A

* .NET CLI:

    1. cd .\Game.Chess
    2. msbuild Game.Chess.csproj /target:rebuild /verbosity:minimal

## Assignment

SE 576: Software Reliability and Testing
Summer 2019

Programming Assignment

Introduction

The goal of this assignment is to give you the opportunity to work with tools that help us become more effective in uncovering software defects.

For this assignment you will implement a move validation engine for the game of chess, use a static analysis tool to help you identify potential defects before you start testing, develop test cases for your program, and collect code coverage data to help you determine the adequacy of your testing effort.

The purpose of the program that you will develop will be to compute all legal moves for a piece on a given chessboard configuration. 

For example, given the board configuration below, your program should compute that the Rook located on square f1 can legally move only to any of the following squares: e1, d1, c1, b1, a1.
 
For a detailed description of all the legal chess moves please refer to https://en.wikipedia.org/wiki/Rules_of_chess 

Input and Output Formats:

Your program should accept as its input a board configuration that contains the position of white pieces, the position of black pieces, and the piece whose possible legal moves are to be computed.

For example, the input for the above board configuration should look something like this:
WHITE: Rf1, Kg1, Pf2, Ph2, Pg3
BLACK: Kb8, Ne8, Pa7, Pb7, Pc7, Ra5
PIECE TO MOVE: Rf1

Please note the use of the capital letters K, Q, R, B, N, and P to identify the King, Queen, Rook, Bishop, Knight, and Pawn respectively.

Given the above input description, your program should produce the following output:
LEGAL MOVES FOR Rf1: e1, d1, c1, b1, a1
