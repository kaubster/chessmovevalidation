﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Game.Chess.Pieces;

namespace Game.Chess
{
    /// <summary>
    /// Creates an instance of ChessRules that encapsualtes code for playing the game of chess.
    /// </summary>
    public partial class ChessRules : IBoardGameRules
    {
        /// <summary>
        /// Single charecter expected when defining the King chess piece.
        /// </summary>
        public static readonly char KING = 'K';

        /// <summary>
        /// Single charecter expected when defining the Queen chess piece.
        /// </summary>
        public static readonly char QUEEN = 'Q';

        /// <summary>
        /// Single charecter expected when defining the Rook chess piece.
        /// </summary>
        public static readonly char ROOK = 'R';

        /// <summary>
        /// Single charecter expected when defining the Bishop chess piece.
        /// </summary>
        public static readonly char BISHOP = 'B';

        /// <summary>
        /// Single charecter expected when defining the Knight chess piece.
        /// </summary>
        public static readonly char KNIGHT = 'N';

        /// <summary>
        /// Single charecter expected when defining the Pawn chess piece.
        /// </summary>
        public static readonly char PAWN = 'P';
        
        /// <summary>
        /// Convenent array containing all supported chess pieces.
        /// </summary>
        public static readonly char[] PIECES = {KING, QUEEN, ROOK, BISHOP, KNIGHT, PAWN};

        /// <summary>
        /// Convenent array containing all valid column names.
        /// </summary>
        public static readonly char[] COLUMNS = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'};

        /// <summary>
        /// Convenent array containing all valid row names.
        /// </summary>
        public static readonly char[] ROWS = {'1', '2', '3', '4', '5', '6', '7', '8'};

        /// <summary>
        /// Dictionary used as a factory for creating an instance of any valid chess piece by name.
        /// </summary>
        private readonly Dictionary<char, AChessPiece> _Pieces = new Dictionary<char, AChessPiece>
        {
            {KING, new King()},
            {QUEEN, new Queen()},
            {ROOK, new Rook()},
            {BISHOP, new Bishop()},
            {KNIGHT, new Knight()},
            {PAWN, new Pawn()}
        };

        /// <summary>
        /// Method for playing a single move in the game of chess that will compute all legal moves 
        /// for a piece on a given chessboard configuration. The chessboard configurations provided via an input file.
        /// </summary>
        /// <param name="inputJson">Name of a valid chess board configuration JSON file within path 
        /// specified by the inputJson parameter. See remarks for file format details.</param>
        /// <param name="inputDir">Path to a valid chess board configuration JSON file.</param>
        /// <returns>Comma delimited string of valid moves.</returns>
        /// <remarks>
        ///  Input file should consist of 3 rows:
        ///   ROW 1: comma seperated list of WHITE pieces formatted as seen below.
        ///   ROW 2: comma seperated list of BLACK pieces formatted as seen below.
        ///   ROW 3: single PIECE TO MOVE formatted as seen below.
        /// 
        ///         Example JSON File Format:    ///     
        ///                 WHITE: Rf1, Kg1, Pf2, Ph2, Pg3, Nf5
        ///                 BLACK: Kb8, Ne8, Pa7, Pb7, Pc7, Ra5
        ///                 PIECE TO MOVE: Rf1
        /// </remarks>
        public string Play(string inputJson, string inputDir)
        {
            var builder = new StringBuilder();

            var turn = ChessTurn.Load(inputJson, inputDir);

            if (turn != null)
            {
                var results = EvaluateOptions(turn);
                builder.AppendLine(results);
            }
            else
            {
                builder.AppendLine($@"Input File: {inputJson} does not exist. Please enter another existing file.");
            }

            return builder.ToString();
        }

        /// <summary>
        /// Evaluates current board configuration specified by ChessTurn object providing a 
        /// list of valid moves given the chess boards configuration and the piece being moved.
        /// </summary>
        /// <param name="turn">ChessTurn object describing a chess board configuration.</param>
        /// <returns>A message describing a comma seperated list of valid moves given the chess boards 
        /// configuration and the piece being moved.
        /// Message example: LEGAL MOVES FOR Rf1: e1,d1,c1,b1,a1</returns>
        public string EvaluateOptions(ChessTurn turn)
        {
            if (null == turn)
            {
#pragma warning disable CA1303 // Do not pass literals as localized parameters - chose not to use string table for now
                throw new ArgumentException("can't be null", nameof(turn));
#pragma warning restore CA1303 // Do not pass literals as localized parameters
            }

            // is the board set and piece to move exists
            if (!turn.IsValid(out var issues))
            {
                return $@"{issues}";
            }

            var board = turn.GenBoard();

            var pos = turn.PieceToMove.ToPosition(turn.PieceToMoveSide);

            if (board != null && (pos?.IsValid() ?? false))
            {
                _Pieces.TryGetValue(pos.Name, out var piece);

                // possible moves
                string[] possibleMoves = piece?.Options(this, board, pos);
                string[] validMovesProneToAttack = Array.Empty<string>();

                // special case king given rubric "Output contains no moves that leave friendly King in check"
                // not removing vulnerable move options from other pieces.
                if(piece is King)
                {
                    // determine if possible moves are prone to attack by enemy pieces
                    validMovesProneToAttack = EvaluateDangerToPosition(turn, possibleMoves);
                }

                // remove positions prone to attack. For example, prevent King from being placed in check.
                var validMoves = possibleMoves.Except(validMovesProneToAttack);

                // report safe moves
                return $@"LEGAL MOVES FOR {turn.PieceToMove}: {string.Join(",", validMoves)}";
            }

            return "ChessPosition not recognized. Please modify the input file.";
        }

        /// <summary>
        /// Evaluates current board configuration to determine if a specified list of possible 
        /// positions are threatend by an enemy piece given a given chess board turn configuration.
        /// </summary>
        /// <param name="turn">ChessTurn object describing a chess board configuration.</param>
        /// <param name="possiblePositions">Array of positions to evaluate.</param>
        /// <returns>A new array containing each position specified within possiblePositions that 
        /// are threatened by enemy positions.</returns>
        public string[] EvaluateDangerToPosition(ChessTurn turn, string[] possiblePositions)
        {
            if(null == turn)
            {
#pragma warning disable CA1303 // Do not pass literals as localized parameters - chose not to use string table for now
                throw new ArgumentException("can't be null", nameof(turn));
#pragma warning restore CA1303 // Do not pass literals as localized parameters
            }

            // is the board set and piece to move exists
            if(!turn.IsValid(out var issues))
            {
                return Array.Empty<string>();
            }

            var board = turn.GenBoard();

            // determine enemy given moving position side
            string[] enemies = turn.Black;
            Sides enemySide = Sides.Black;
            if(turn.PieceToMoveSide == Sides.Black)
            {
                enemies = turn.White;
                enemySide = Sides.White;
            }

            HashSet<string> dangerousMoves = new HashSet<string>();

            // Determine possible moves by each enemy
            foreach(var enemy in enemies)
            {
                var pos = enemy.ToPosition(enemySide);

                if(board != null && (pos?.IsValid() ?? false))
                {
                    _Pieces.TryGetValue(pos.Name, out var piece);

                    string[] validMoves = piece?.Options(this, board, pos);

                    // Determine if valid enemy moves intersect possible positions.
                    var moves = possiblePositions.Intersect(validMoves);
                    if(moves != null && moves.Count() > 0)
                    {
                        // Found colliding positions, prone to attack
                        dangerousMoves.UnionWith(moves); // avoid duplicates
                    }
                }
            }

            // Report moves prone to attack
            return dangerousMoves.ToArray();
        }
    }
}