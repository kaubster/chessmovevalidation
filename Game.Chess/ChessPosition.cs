﻿using System;
using System.Globalization;
using System.Linq;

namespace Game.Chess
{
    /// <summary>
    /// Object for describing a position on a chess board. Position consists of piece name, location and side.
    /// </summary>
    public class ChessPosition : ICloneable
    {
        /// <summary>
        /// Constructor for creating an instance of ChessPosition.
        /// </summary>
        /// <param name="boardLocation">Position on board formatted as a 3 charecter string. See remarks for formatting details.</param>
        /// <param name="side">The side assocated with this location.</param>
        /// <remarks>Example position format: Rf1. 
        /// First Charecter: K, Q, R, B, N, or P to identify the King, Queen, Rook, Bishop, Knight, and Pawn respectively.
        /// Second Charecter: a, b, c, d, e, f or h defining column.
        /// Third Charecter: 1, 2, 3, 4, 5, 6, 7, 8 defining row.
        /// </remarks>
        public ChessPosition(string boardLocation, ChessRules.Sides side)
        {
            if (string.IsNullOrEmpty(boardLocation) || boardLocation.Length != 3)
                throw new ArgumentException(boardLocation);

            Name = char.ToUpper(boardLocation[0], CultureInfo.CreateSpecificCulture("en-US"));
            Location = $@"{boardLocation[1]}{boardLocation[2]}".ToLocation();
            Side = side;
        }

        /// <summary>
        /// The piece name where K, Q, R, B, N, or P to identify the King, Queen, Rook, Bishop, Knight, and Pawn respectively.
        /// </summary>
        public char Name { get; set; }

        /// <summary>
        /// Location on the board.
        /// </summary>
        public Location Location { get; set; }

        /// <summary>
        /// Side associated with this location.
        /// </summary>
        public ChessRules.Sides Side { get; internal set; }

        /// <summary>
        /// Creates a new object that is a copy of the current instance.
        /// </summary>
        /// <returns>A new object that is a copy of this instance.</returns>
        public object Clone()
        {
            return new ChessPosition(ToString(), Side);
        }

        /// <summary>
        /// Provides string describing this positions piece name, column and row.
        /// For example, Rook in column f row 1 string will be "Rf1".
        /// </summary>
        /// <returns>String formatted NameColumnRow.</returns>
        public override string ToString()
        {
            return $@"{Name}{Location.Column}{Location.Row}";
        }

        /// <summary>
        /// Verifies current class properties are well defined.
        /// </summary>
        /// <returns>True when location, piece name, column and row are valid.</returns>
        public bool IsValid()
        {
            return Location.IsValid() &&
                   ChessRules.PIECES.Contains(Name) &&
                   ChessRules.COLUMNS.Contains(Location.Column) &&
                   ChessRules.ROWS.Contains(Location.Row);
        }

        /// <summary>
        /// Overridden equals operator for comparing ChessPosition objects.
        /// </summary>
        /// <param name="obj1">Left object to compare.</param>
        /// <param name="obj2">Right object to compare.</param>
        /// <returns>True when ChessPosition objects are the equivalent.</returns>
        public static bool operator ==(ChessPosition obj1, ChessPosition obj2)
        {
            if (Equals(obj1, null))
            {
                if (Equals(obj2, null)) return true;

                return false;
            }

            return obj1.Equals(obj2);
        }

        /// <summary>
        /// Overridden not equals operator for comparing ChessPosition objects.
        /// </summary>
        /// <param name="obj1">Left object to compare.</param>
        /// <param name="obj2">Right object to compare.</param>
        /// <returns>True when ChessPosition objects are not equivalent.</returns>
        public static bool operator !=(ChessPosition obj1, ChessPosition obj2)
        {
            return !(obj1 == obj2);
        }

        /// <summary>
        /// Overridden equals method for determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">Object  object to compare.</param>
        /// <returns>True when specified ChessPosition is equivalent to the current object.</returns>
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType()) return false;

            var piece = obj as ChessPosition;
            return piece != null &&
                   Name == piece.Name &&
                   Location == piece.Location;
		}
                
        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            return HashCode.Combine(Name, Location, Side);
        }

        /// <summary>
        /// Determines ChessPostion diagonally left and up from the specified ChessPosition.
        /// </summary>
        /// <param name="cPos">Starting position.</param>
        /// <param name="tileCount">Number of tile movements. Default is 1 tile.</param>
        /// <returns>The ChessPosition diagonally left and up given the specified tile count.</returns>
        public static ChessPosition MoveLeftUpDiagonal(ChessPosition cPos, ushort tileCount = 1)
        {
            if(cPos == null)
            {
                throw new ArgumentNullException(nameof(cPos));
            }

            var clone = cPos.Clone() as ChessPosition;
            clone.Location.RowIndex += tileCount;
            clone.Location.ColumnIndex -= tileCount;

            // Success when Row and Column has changed. Either same indicates edge of board reached.
            if (clone.Location.RowIndex != cPos.Location.RowIndex &&
                clone.Location.ColumnIndex != cPos.Location.ColumnIndex) return clone;

            // Move not possible. Return original.
            return cPos;
        }

        /// <summary>
        /// Determines ChessPostion diagonally left and down from the specified ChessPosition.
        /// </summary>
        /// <param name="cPos">Starting position.</param>
        /// <param name="tileCount">Number of tile movements. Default is 1 tile.</param>
        /// <returns>The ChessPosition diagonally left and down given the specified tile count.</returns>
        public static ChessPosition MoveLeftDownDiagonal(ChessPosition cPos, ushort tileCount = 1)
        {
            if(cPos == null)
            {
                throw new ArgumentNullException(nameof(cPos));
            }

            var clone = cPos.Clone() as ChessPosition;
            clone.Location.RowIndex -= tileCount;
            clone.Location.ColumnIndex -= tileCount;

            // Success when Row and Column has changed. Either same indicates edge of board reached.
            if (clone.Location.RowIndex != cPos.Location.RowIndex &&
                clone.Location.ColumnIndex != cPos.Location.ColumnIndex) return clone;

            // Move not possible. Return original.
            return cPos;
        }

        /// <summary>
        /// Determines ChessPostion diagonally right and up from the specified ChessPosition.
        /// </summary>
        /// <param name="cPos">Starting position.</param>
        /// <param name="tileCount">Number of tile movements. Default is 1 tile.</param>
        /// <returns>The ChessPosition diagonally right and up given the specified tile count.</returns>
        public static ChessPosition MoveRightUpDiagonal(ChessPosition cPos, ushort tileCount = 1)
        {
            if(cPos == null)
            {
                throw new ArgumentNullException(nameof(cPos));
            }

            var clone = cPos.Clone() as ChessPosition;
            clone.Location.RowIndex += tileCount;
            clone.Location.ColumnIndex += tileCount;

            // Success when Row and Column has changed. Either same indicates edge of board reached.
            if (clone.Location.RowIndex != cPos.Location.RowIndex &&
                clone.Location.ColumnIndex != cPos.Location.ColumnIndex) return clone;

            // Move not possible. Return original.
            return cPos;
        }

        /// <summary>
        /// Determines ChessPostion diagonally right and down from the specified ChessPosition.
        /// </summary>
        /// <param name="cPos">Starting position.</param>
        /// <param name="tileCount">Number of tile movements. Default is 1 tile.</param>
        /// <returns>The ChessPosition diagonally right and down given the specified tile count.</returns>
        public static ChessPosition MoveRightDownDiagonal(ChessPosition cPos, ushort tileCount = 1)
        {
            if(cPos == null)
            {
                throw new ArgumentNullException(nameof(cPos));
            }

            var clone = cPos.Clone() as ChessPosition;
            clone.Location.RowIndex -= tileCount;
            clone.Location.ColumnIndex += tileCount;

            // Success when Row and Column has changed. Either same indicates edge of board reached.
            if (clone.Location.RowIndex != cPos.Location.RowIndex &&
                clone.Location.ColumnIndex != cPos.Location.ColumnIndex) return clone;

            // Move not possible. Return original.
            return cPos;
        }

        /// <summary>
        /// Determines ChessPostion up from the specified ChessPosition.
        /// </summary>
        /// <param name="cPos">Starting position.</param>
        /// <param name="tileCount">Number of tile movements. Default is 1 tile.</param>
        /// <returns>The ChessPosition up given the specified tile count.</returns>
        public static ChessPosition MoveUp(ChessPosition cPos, ushort tileCount = 1)
        {
            if(cPos == null)
            {
                throw new ArgumentNullException(nameof(cPos));
            }

            var clone = cPos.Clone() as ChessPosition;
            clone.Location.RowIndex += tileCount;
            //clone.Location.ColumnIndex += 0;

            // Success when changed. Same indicates edge of board reached.
            if (clone.Location.RowIndex != cPos.Location.RowIndex) return clone;

            // Move not possible. Return original.
            return cPos;
        }

        /// <summary>
        /// Determines ChessPostion down from the specified ChessPosition.
        /// </summary>
        /// <param name="cPos">Starting position.</param>
        /// <param name="tileCount">Number of tile movements. Default is 1 tile.</param>
        /// <returns>The ChessPosition down given the specified tile count.</returns>
        public static ChessPosition MoveDown(ChessPosition cPos, ushort tileCount = 1)
        {
            if(cPos == null)
            {
                throw new ArgumentNullException(nameof(cPos));
            }

            var clone = cPos.Clone() as ChessPosition;
            clone.Location.RowIndex -= tileCount;
            //clone.Location.ColumnIndex += 0;

            // Success when changed. Same indicates edge of board reached.
            if (clone.Location.RowIndex != cPos.Location.RowIndex) return clone;

            // Move not possible. Return original.
            return cPos;
        }

        /// <summary>
        /// Determines ChessPostion right from the specified ChessPosition.
        /// </summary>
        /// <param name="cPos">Starting position.</param>
        /// <param name="tileCount">Number of tile movements. Default is 1 tile.</param>
        /// <returns>The ChessPosition right given the specified tile count.</returns>
        public static ChessPosition MoveRight(ChessPosition cPos, ushort tileCount = 1)
        {
            if(cPos == null)
            {
                throw new ArgumentNullException(nameof(cPos));
            }

            var clone = cPos.Clone() as ChessPosition;
            //clone.Location.RowIndex += 0;
            clone.Location.ColumnIndex += tileCount;

            // Success when changed. Same indicates edge of board reached.
            if (clone.Location.ColumnIndex != cPos.Location.ColumnIndex) return clone;

            // Move not possible. Return original.
            return cPos;
        }

        /// <summary>
        /// Determines ChessPostion left from the specified ChessPosition.
        /// </summary>
        /// <param name="cPos">Starting position.</param>
        /// <param name="tileCount">Number of tile movements. Default is 1 tile.</param>
        /// <returns>The ChessPosition left given the specified tile count.</returns>
        public static ChessPosition MoveLeft(ChessPosition cPos, ushort tileCount = 1)
        {
            if(cPos == null)
            {
                throw new ArgumentNullException(nameof(cPos));
            }

            var clone = cPos.Clone() as ChessPosition;
            //clone.Location.RowIndex -= 0;
            clone.Location.ColumnIndex -= tileCount;

            // Success when changed. Same indicates edge of board reached.
            if (clone.Location.ColumnIndex != cPos.Location.ColumnIndex) return clone;

            // Move not possible. Return original.
            return cPos;
        }
    }
}