﻿

using System;
/**
* Static analysis notes:
*
* Class name must match file name
*/
namespace Game.Chess
{
    public partial class ChessRules
    {
        /// <summary>
        /// Enumeration describing chess sides.
        /// </summary>
        [Flags]
        public enum Sides
        {
            Undefined,
            White,
            Black
        }
    }
}