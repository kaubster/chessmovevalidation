﻿using System.Collections.Generic;

namespace Game.Chess.Pieces
{
    /// <summary>
    ///     An object that describes the Pawn chess piece.
    /// </summary>
    public class Pawn : AChessPiece
    {
        /// <summary>
        /// Overriden abstract method allowing piece to customize its strategy options.
        /// </summary>
        /// <param name="board">An instance of ChessPosition </param>
        /// <param name="pos">Instance of ChessPosition to be evaluated.</param>
        /// <returns>Array of strings describing possible valid move positions.</returns>
        public override string[] Options(ChessRules rules, ChessPosition[,] board, ChessPosition pos)
        {
            if(pos == null)
            {
                throw new System.ArgumentNullException(nameof(pos));
            }

            var options = new List<string>();

            // Black row = 7, allow double move
            // White row = 2, allow double move
            var doubleRow = pos.Side == ChessRules.Sides.Black ? 7 - 1 /*Black*/ : 2 - 1;

            // Black forward = down
            // White forward = up
            var forward = pos.Side == ChessRules.Sides.Black ? ChessRules.Moves.Down /*Black*/ : ChessRules.Moves.Up;

            /// Move forward to the unoccupied square immediately in front of it on the same file, 
            var finding = Move(board, forward, pos, 1 /*move 1 tile*/);

            if (!string.IsNullOrEmpty(finding)) options.Add(finding);

            /// or on its first move it can advance two squares along the same file, provided both squares are unoccupied (black dots in the diagram);
            if (pos.Location.RowIndex == doubleRow)
            {
                // TO DO: Mainain list of pawns that have had a single move.
                // Double move can only happen once per pawn piece.
                finding = Move(board, forward, pos, 2 /*move 2 tile*/);
                if (!string.IsNullOrEmpty(finding)) options.Add(finding);
            }

            /// or the pawn can capture an opponent's piece on a square diagonally in front of it on an adjacent file, by moving to that square (black "x"s)

            // Attack diagonal left
            var mPos = Move(ChessRules.Moves.Left, pos, 1 /*move 2 tile*/);
            finding = string.Empty;
            // When same, indication movement was not possible since we ran into edge of the board.
            if (mPos != pos) finding = Attack(board, forward, mPos, 1 /*move 1 tile*/);

            if (!string.IsNullOrEmpty(finding)) options.Add(finding);

            // Attack diagonal right
            mPos = Move(ChessRules.Moves.Right, pos, 1 /*move 2 tile*/);
            finding = string.Empty;
            // When same, indication movement was not possible since we ran into edge of the board.
            if (mPos != pos) finding = Attack(board, forward, mPos, 1 /*move 1 tile*/);

            if (!string.IsNullOrEmpty(finding)) options.Add(finding);

            return options.ToArray();
        }
    }
}