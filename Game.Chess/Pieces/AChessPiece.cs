﻿using System.Collections.Generic;

namespace Game.Chess.Pieces
{
    public abstract class AChessPiece
    {
        /// <summary>
        /// Abstract method allowing each piece to customize its strategy options.
        /// </summary>
        /// <param name="board">An instance of ChessPosition </param>
        /// <param name="pos">Instance of ChessPosition to be evaluated.</param>
        /// <returns>Array of strings describing possible valid move positions.</returns>
        public abstract string[] Options(ChessRules rules, ChessPosition[,] board, ChessPosition pos);

        protected static ChessPosition Move(ChessRules.Moves direction, ChessPosition start, ushort tileCount = 1)
        {
            switch (direction)
            {
                case ChessRules.Moves.LeftUpDiagonal:
                    return ChessPosition.MoveLeftUpDiagonal(start, tileCount);
                case ChessRules.Moves.Up:
                    return ChessPosition.MoveUp(start, tileCount);
                case ChessRules.Moves.RightUpDiagonal:
                    return ChessPosition.MoveRightUpDiagonal(start, tileCount);
                case ChessRules.Moves.Right:
                    return ChessPosition.MoveRight(start, tileCount);
                case ChessRules.Moves.RightDownDiagonal:
                    return ChessPosition.MoveRightDownDiagonal(start, tileCount);
                case ChessRules.Moves.Down:
                    return ChessPosition.MoveDown(start, tileCount);
                case ChessRules.Moves.LeftDownDiagonal:
                    return ChessPosition.MoveLeftDownDiagonal(start, tileCount);
                case ChessRules.Moves.Left:
                    return ChessPosition.MoveLeft(start, tileCount);
            }

            return start;
        }

        protected static string Move(ChessPosition[,] board, ChessRules.Moves direction,
            ChessPosition startPos, ushort tileCount = 1)
        {
            if(board == null)
            {
                throw new System.ArgumentNullException(nameof(board));
            }

            if(startPos == null)
            {
                throw new System.ArgumentNullException(nameof(startPos));
            }

            ChessPosition mPos = Move(direction, startPos, tileCount);

            // When same, indication movement was not possible since we ran into edge of the board.
            if (mPos != null && mPos != startPos)
            {
                var bPos = board[mPos.Location.ColumnIndex, mPos.Location.RowIndex];
                ContinueSearching(bPos, mPos, out var finding);
                return finding;
            }

            return string.Empty;
        }

        protected static bool ContinueSearching(ChessPosition bPos, ChessPosition cPos, out string options)
        {
            if(cPos == null)
            {
                throw new System.ArgumentNullException(nameof(cPos));
            }

            options = string.Empty;
            if (bPos == null)
            {
                // found an empty space, remember it.
                options = cPos.Location.ToString();
                return true;
            }

            if (bPos.Side == cPos.Side) return false;

            // bPos.Side != cPos.Side, we've run into an anemy.
            // Blocked, by enemy. Get um..
            // Note possible move since this enemy is vulnerable.
            options = $@"{bPos.Location}"; // enemy is vulnerable
            return false;
        }

        protected static string Attack(ChessPosition[,] board, ChessRules.Moves direction,
            ChessPosition startPos, ushort tileCount = 1)
        {
            if(board == null)
            {
                throw new System.ArgumentNullException(nameof(board));
            }

            if(startPos == null)
            {
                throw new System.ArgumentNullException(nameof(startPos));
            }

            ChessPosition mPos = Move(direction, startPos, tileCount);

            // When same, indication movement was not possible since we ran into edge of the board.
            if (mPos != null && mPos != startPos)
            {
                var bPos = board[mPos.Location.ColumnIndex, mPos.Location.RowIndex];
                AttackSearching(bPos, mPos, out var finding);
                return finding;
            }

            return string.Empty;
        }

        private static bool AttackSearching(ChessPosition bPos, ChessPosition cPos, out string options)
        {
            options = string.Empty;
            if (bPos == null) return false;

            if (bPos.Side == cPos.Side)
            {
                // Blocked, by friendly. No go.
                return false;
            }

            // bPos.Side != cPos.Side, we've run into an anemy.
            // Blocked, by enemy. Get um..
            // Note possible move since this enemy is vulnerable.
            options = $@"{bPos.Location}"; // enemy is vulnerable
            return true;
        }
    }
}