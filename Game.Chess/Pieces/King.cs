﻿using System.Collections.Generic;

namespace Game.Chess.Pieces
{
    /// <summary>
    ///     An object that describes the King chess piece.
    /// </summary>
    public class King : AChessPiece
    {
        /// <summary>
        /// Overriden abstract method allowing piece to customize its strategy options.
        /// </summary>
        /// <param name="board">An instance of ChessPosition </param>
        /// <param name="pos">Instance of ChessPosition to be evaluated.</param>
        /// <returns>Array of strings describing possible valid move positions.</returns>
        public override string[] Options(ChessRules rules, ChessPosition[,] board, ChessPosition pos)
        {
            var options = new List<string>();

            // Move one in each direction.
            // Dont record where we've hit an edge of the board.
            var finding = Move(board, ChessRules.Moves.LeftDownDiagonal, pos, 1 /*move 1 tile*/);

            if (!string.IsNullOrEmpty(finding)) options.Add(finding);

            finding = Move(board, ChessRules.Moves.Left, pos, 1 /*move 1 tile*/);

            if (!string.IsNullOrEmpty(finding)) options.Add(finding);

            finding = Move(board, ChessRules.Moves.LeftUpDiagonal, pos, 1 /*move 1 tile*/);

            if (!string.IsNullOrEmpty(finding)) options.Add(finding);

            finding = Move(board, ChessRules.Moves.Up, pos, 1 /*move 1 tile*/);

            if (!string.IsNullOrEmpty(finding)) options.Add(finding);

            finding = Move(board, ChessRules.Moves.RightUpDiagonal, pos, 1 /*move 1 tile*/);

            if (!string.IsNullOrEmpty(finding)) options.Add(finding);

            finding = Move(board, ChessRules.Moves.Right, pos, 1 /*move 1 tile*/);

            if (!string.IsNullOrEmpty(finding)) options.Add(finding);

            finding = Move(board, ChessRules.Moves.RightDownDiagonal, pos, 1 /*move 1 tile*/);

            if (!string.IsNullOrEmpty(finding)) options.Add(finding);

            finding = Move(board, ChessRules.Moves.Down, pos, 1 /*move 1 tile*/);

            if (!string.IsNullOrEmpty(finding)) options.Add(finding);

            return options.ToArray();
        }
    }
}