﻿using System.Collections.Generic;

namespace Game.Chess.Pieces
{
    /// <summary>
    ///     An object that describes the Knight chess piece.
    /// </summary>
    public class Knight : AChessPiece
    {
        /// <summary>
        /// Overriden abstract method allowing piece to customize its strategy options.
        /// </summary>
        /// <param name="board">An instance of ChessPosition </param>
        /// <param name="pos">Instance of ChessPosition to be evaluated.</param>
        /// <returns>Array of strings describing possible valid move positions.</returns>
        public override string[] Options(ChessRules rules, ChessPosition[,] board, ChessPosition pos)
        {
            var options = new List<string>();

            // The knight is the only piece that can leap over other pieces.
            // That is why the Knight moves directly to destination without bothering
            // to determine if blocked by friend or foe.

            // Check left 2, up 1.
            /*no*/
            var mPos = Move(ChessRules.Moves.Left, pos, 2 /*move 2 tile*/);
            var finding = Move(board, ChessRules.Moves.Up, mPos, 1 /*move 1 tile*/);

            if (!string.IsNullOrEmpty(finding)) options.Add(finding);

            // Check left 1, up 2.
            mPos = Move(ChessRules.Moves.Left, pos, 1 /*move 1 tile*/);
            finding = Move(board, ChessRules.Moves.Up, mPos, 2 /*move 2 tile*/);

            /*no*/
            if (!string.IsNullOrEmpty(finding)) options.Add(finding);

            // Check right 2, up 1.
            mPos = Move(ChessRules.Moves.Right, pos, 2 /*move 2 tile*/);
            /*no*/
            finding = Move(board, ChessRules.Moves.Up, mPos, 1 /*move 1 tile*/);

            if (!string.IsNullOrEmpty(finding)) options.Add(finding);

            // Check right 1, up 2.
            /*no*/
            mPos = Move(ChessRules.Moves.Right, pos, 1 /*move 1 tile*/);
            finding = Move(board, ChessRules.Moves.Up, mPos, 2 /*move 2 tile*/);

            if (!string.IsNullOrEmpty(finding)) options.Add(finding);

            // Check right 2, down 1
            /*yes*/
            mPos = Move(ChessRules.Moves.Right, pos, 2 /*move 2 tile*/);
            finding = Move(board, ChessRules.Moves.Down, mPos, 1 /*move 1 tile*/);

            if (!string.IsNullOrEmpty(finding)) options.Add(finding);

            // Check right 1, down 2
            /*yes*/
            mPos = Move(ChessRules.Moves.Right, pos, 1 /*move 1 tile*/);
            finding = Move(board, ChessRules.Moves.Down, mPos, 2 /*move 2 tile*/);

            if (!string.IsNullOrEmpty(finding)) options.Add(finding);

            // Check left 2, down 1
            /*no*/
            mPos = Move(ChessRules.Moves.Left, pos, 2 /*move 2 tile*/);
            finding = Move(board, ChessRules.Moves.Down, mPos, 1 /*move 1 tile*/);

            if (!string.IsNullOrEmpty(finding)) options.Add(finding);

            // Check left 1, down 2
            /*yes*/
            mPos = Move(ChessRules.Moves.Left, pos, 1 /*move 1 tile*/);
            finding = Move(board, ChessRules.Moves.Down, mPos, 2 /*move 2 tile*/);

            if (!string.IsNullOrEmpty(finding)) options.Add(finding);

            return options.ToArray();
        }
    }
}