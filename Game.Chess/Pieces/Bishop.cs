﻿using System.Collections.Generic;

namespace Game.Chess.Pieces
{
    /// <summary>
    ///     An object that describes the bishop chess piece.
    /// </summary>
    public class Bishop : AChessPiece
    {
        /// <summary>
        /// Overriden abstract method allowing piece to customize its strategy options.
        /// </summary>
        /// <param name="board">An instance of ChessPosition </param>
        /// <param name="pos">Instance of ChessPosition to be evaluated.</param>
        /// <returns>Array of strings describing possible valid move positions.</returns>
        public override string[] Options(ChessRules rules, ChessPosition[,] board, ChessPosition pos)
        {
            if(board == null)
            {
                throw new System.ArgumentNullException(nameof(board));
            }

            var options = new List<string>();
            var lPos = pos;
            var cPos = pos;

            // Moves any number of vacant squares in any diagonal directions.
            // go until we've hit an edge of the board.
            while ((cPos = ChessPosition.MoveLeftDownDiagonal(cPos)) != lPos)
            {
                var bPos = board[cPos.Location.ColumnIndex, cPos.Location.RowIndex];

                var done = ContinueSearching(bPos, cPos, out var finding);

                if (!string.IsNullOrEmpty(finding)) options.Add(finding);

                if (!done) break;

                lPos = cPos;
            }

            lPos = pos;
            cPos = pos;
            while ((cPos = ChessPosition.MoveLeftUpDiagonal(cPos)) != lPos)
            {
                var bPos = board[cPos.Location.ColumnIndex, cPos.Location.RowIndex];

                var done = ContinueSearching(bPos, cPos, out var finding);

                if (!string.IsNullOrEmpty(finding)) options.Add(finding);

                if (!done) break;

                lPos = cPos;
            }

            lPos = pos;
            cPos = pos;
            // Move any number of squares along a file.
            // go until we've hit an edge of the board.
            while ((cPos = ChessPosition.MoveRightUpDiagonal(cPos)) != lPos)
            {
                var bPos = board[cPos.Location.ColumnIndex, cPos.Location.RowIndex];

                var done = ContinueSearching(bPos, cPos, out var finding);

                if (!string.IsNullOrEmpty(finding)) options.Add(finding);

                if (!done) break;

                lPos = cPos;
            }

            lPos = pos;
            cPos = pos;
            while ((cPos = ChessPosition.MoveRightDownDiagonal(cPos)) != lPos)
            {
                var bPos = board[cPos.Location.ColumnIndex, cPos.Location.RowIndex];

                var done = ContinueSearching(bPos, cPos, out var finding);

                if (!string.IsNullOrEmpty(finding)) options.Add(finding);

                if (!done) break;

                lPos = cPos;
            }

            return options.ToArray();
        }
    }
}