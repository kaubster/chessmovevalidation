﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using Game.Chess;
using Newtonsoft.Json;

public class ChessTurn
{
    /// <summary>
    /// Array of black piece positions. See ChessPosition class for position formatting details.
    /// </summary>
    [JsonProperty("BLACK")]
    public string[] Black { get; set; } = Array.Empty<string>();

    /// <summary>
    /// Array of white piece positions. See ChessPosition class for position formatting details.
    /// </summary>
    [JsonProperty("WHITE")]
    public string[] White { get; set; } = Array.Empty<string>();

    /// <summary>
    /// A single piece to be moved when this turn is evaluted. See ChessPosition class for position formatting details.
    /// </summary>
    [JsonProperty("PIECE TO MOVE")] public string PieceToMove { get; set; } = string.Empty; // "Pc2";

    /// <summary>
    /// Side associated with PIECE TO MOVE.
    /// </summary>
    public ChessRules.Sides PieceToMoveSide { get; internal set; }

    /// <summary>
    /// Loads instance of ChessTurn from a valid chess board configuration JSON file. 
    /// </summary>
    /// <param name="inputJson">Name of a valid chess board configuration JSON file within path 
    /// specified by the inputJson parameter. See remarks for file format details.</param>
    /// <param name="inputDir">Path to a valid chess board configuration JSON file.</param>
    /// <returns>Comma delimited string of valid moves.</returns>
    /// <returns>When successfull an instance of ChessTurn will be returned containing valid BLACK, WHITE, PIECE TO MOVE and Side.</returns>
    /// <remarks>
    ///  Input file should consist of 3 rows:
    ///   ROW 1: comma seperated list of WHITE pieces formatted as seen below.
    ///   ROW 2: comma seperated list of BLACK pieces formatted as seen below.
    ///   ROW 3: signle PIECE TO MOVE formatted as seen below.
    /// 
    ///         Example JSON File Format:    ///     
    ///                 WHITE: Rf1, Kg1, Pf2, Ph2, Pg3, Nf5
    ///                 BLACK: Kb8, Ne8, Pa7, Pb7, Pc7, Ra5
    ///                 PIECE TO MOVE: Rf1
    /// 
    ///  Check ChessTurn.IsValid() to verify configuration file.</remarks>
    public static ChessTurn Load(string inputJson, string inputDir)
    {
        if (string.IsNullOrEmpty(inputJson)) throw new ArgumentNullException(nameof(inputJson));
        if (string.IsNullOrEmpty(inputDir)) throw new ArgumentNullException(nameof(inputDir));

        var tInput = inputJson;
        if (!File.Exists(tInput) && !File.Exists(tInput + ".json"))
            tInput = Path.Combine(Directory.GetCurrentDirectory(), inputDir, inputJson);

        if (File.Exists(tInput))
            return JsonConvert.DeserializeObject<ChessTurn>(File.ReadAllText(tInput));
        if (File.Exists(tInput + ".json"))
            return JsonConvert.DeserializeObject<ChessTurn>(File.ReadAllText(tInput + ".json"));

        return new ChessTurn(); // Invalid null pattern
    }

    /// <summary>
    /// Validates current ChessTurn settings verifying the board set and valid piece to move exists.
    /// </summary>
    /// <param name="issues">Warning and errors found. Empty otherwise.</param>
    /// <returns>True when board layout is valid piece to move exists on board.</returns>
    public bool IsValid(out string issues)
    {
        var wBuilder = new StringBuilder();
        var eBuilder = new StringBuilder();
        issues = string.Empty;

        // remove dups
        var result = White.Union(Black).ToList();

        // ensure there were no duplicates
        if (result.Count != White.Length + Black.Length)
            wBuilder.AppendLine(@"Warning: Duplicates detected. Duplicate values ignored.");

        // Ensure piece to be moved is on the board
        if (!result.Contains(PieceToMove))
        {
            eBuilder.AppendLine(@"Error: Piece To Move does not appear upon the board.");
        }

        if (White.Contains(PieceToMove))
        {
            PieceToMoveSide = ChessRules.Sides.White;
        }
        else if (Black.Contains(PieceToMove))
        {
            PieceToMoveSide = ChessRules.Sides.Black;
        }

        // Ensure black and white locations are valid board locations.
        foreach (var p in White)
        {
            if (!p.ToPosition(ChessRules.Sides.White).IsValid())
            {
                eBuilder.AppendLine($@"Error: White {p} is not formatted correctly.");
            }
        }

        foreach (var p in Black)
        {
            if (!p.ToPosition(ChessRules.Sides.Black).IsValid())
            {
                eBuilder.AppendLine($@"Error: Black {p} is not formatted correctly.");
            }
        }

        wBuilder.Append(eBuilder);

        issues = wBuilder.ToString();

        if (eBuilder.Length > 0)
        {
            return false;
        }

        return true;
    }

    /// <summary>
    /// Creates two dimensional array describing the chess board configuration. 
    /// Black and white positions are placed on the board.
    /// </summary>
    /// <returns>Two dimensional chess board configuration.</returns>
    public ChessPosition[,] GenBoard()
    {
        var board = new ChessPosition[8, 8];

        foreach (var pos in White)
        {
            var cPos = pos.ToPosition(ChessRules.Sides.White);
            board[cPos.Location.ColumnIndex, cPos.Location.RowIndex] = cPos;
        }

        foreach (var pos in Black)
        {
            var cPos = pos.ToPosition(ChessRules.Sides.Black);
            board[cPos.Location.ColumnIndex, cPos.Location.RowIndex] = cPos;
        }

        return board;
    }
}