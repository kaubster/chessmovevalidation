﻿using System;

namespace Game.Chess
{
    public static class GameExtensions
    {
        /// <summary>
        /// Convenience extension for turning a well formatted board position string to an instance of Game.Chess.ChessPosition.
        /// </summary>
        /// <param name="boardPosition">Well formatted board position string. See ChessPosition class for position string formatting details.</param>
        /// <param name="side">Side associated with this position.</param>
        /// <returns>Instance of ChessPosition for the provided board position string.</returns>
        public static ChessPosition ToPosition(this string boardPosition, ChessRules.Sides side)
        {
            if (string.IsNullOrEmpty(boardPosition)) throw new ArgumentException(boardPosition);

            return new ChessPosition(boardPosition, side);
        }

        /// <summary>
        /// Convenience extension for turning a well formatted board location string to an instance of Game.Location.
        /// </summary>
        /// <param name="boardPosition">Well formatted board position string. See Game.Location class for location string formatting details.</param>
        /// <param name="side">Side associated with this position.</param>
        /// <returns>Instance of ChessPosition for the provided board locaion string.</returns>
        public static Location ToLocation(this string boardLocation)
        {
            if (string.IsNullOrEmpty(boardLocation)) throw new ArgumentException(boardLocation);

            return new Location(boardLocation);
        }
    }
}