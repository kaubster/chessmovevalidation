﻿using System;

namespace Game.Chess
{
    /// <summary>
    /// Program for playing a single turn in the game of chess.
    /// </summary>
    public static class Program
    {
#pragma warning disable CA1707 // Identifiers should not contain underscores
        public const string INPUT_DATA_FOLDER_NAME = "InputData";
#pragma warning restore CA1707 // Identifiers should not contain underscores

        private static void Main(string[] args)
        {
            MainEntry(args);
        }

        public static void MainEntry(string[] args)
        {
            // TO DO: Add dependency injection framework.
            /// Instance of the game chess rules.
            IBoardGameRules m_Rules = new ChessRules();

            if(args != null && args.Length == 1)
            {
                // Argument should be a file, if not Play will provide errors messages
                Console.WriteLine(m_Rules.Play(args[0], INPUT_DATA_FOLDER_NAME));
            } else
            {
#pragma warning disable CA1303 // Do not pass literals as localized parameters - chose not to use resource table for now
                Console.WriteLine("Enter name of InputFile [ENTER]");
#pragma warning restore CA1303 // Do not pass literals as localized parameters
            }
        }
    }
}