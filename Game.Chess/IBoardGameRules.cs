﻿namespace Game.Chess
{
    /// <summary>
    /// Provides contract for generating game specific board rules.
    /// </summary>
    public interface IBoardGameRules
    {
        /// <summary>
        /// Method for playing a single move in the game given the configuration specified by a configuration file.
        /// </summary>
        /// <param name="inputJson">Name of a valid game board configuration file within path 
        /// specified by the inputJson parameter.</param>
        /// <param name="inputDir">Path to a valid game board configuration file.</param>
        /// <returns>Comma delimited string of valid moves.</returns>
        string Play(string input, string inputDir);
    }
}