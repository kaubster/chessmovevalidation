﻿// This file is used by Code Analysis to maintain SuppressMessage 
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given 
// a specific target and scoped to a namespace, type, member, etc.

using System.Diagnostics.CodeAnalysis;


[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1819:Properties should not return arrays", Justification = "<Pending>", Scope = "member", Target = "~P:ChessTurn.White")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1819:Properties should not return arrays", Justification = "<Pending>", Scope = "member", Target = "~P:ChessTurn.Black")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1814:Prefer jagged arrays over multidimensional", Justification = "<Pending>", Scope = "member", Target = "~M:Game.Chess.Pieces.AChessPiece.Options(Game.Chess.ChessPosition[,],Game.Chess.ChessPosition)~System.String[]")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1814:Prefer jagged arrays over multidimensional", Justification = "<Pending>", Scope = "member", Target = "~M:Game.Chess.Pieces.AChessPiece.Move(System.Collections.Generic.List{System.String},Game.Chess.ChessPosition[,],Game.Chess.ChessRules.Moves,Game.Chess.ChessPosition,System.UInt16)~System.String")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1814:Prefer jagged arrays over multidimensional", Justification = "<Pending>", Scope = "member", Target = "~M:ChessTurn.GenBoard~Game.Chess.ChessPosition[,]")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1814:Prefer jagged arrays over multidimensional", Justification = "<Pending>", Scope = "member", Target = "~M:Game.Chess.Pieces.AChessPiece.Attack(System.Collections.Generic.List{System.String},Game.Chess.ChessPosition[,],Game.Chess.ChessRules.Moves,Game.Chess.ChessPosition,System.UInt16)~System.String")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1814:Prefer jagged arrays over multidimensional", Justification = "<Pending>", Scope = "member", Target = "~M:Game.Chess.Pieces.AChessPiece.Move(Game.Chess.ChessPosition[,],Game.Chess.ChessRules.Moves,Game.Chess.ChessPosition,System.UInt16)~System.String")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1814:Prefer jagged arrays over multidimensional", Justification = "<Pending>", Scope = "member", Target = "~M:Game.Chess.Pieces.AChessPiece.Attack(Game.Chess.ChessPosition[,],Game.Chess.ChessRules.Moves,Game.Chess.ChessPosition,System.UInt16)~System.String")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1814:Prefer jagged arrays over multidimensional", Justification = "<Pending>", Scope = "member", Target = "~M:Game.Chess.Pieces.AChessPiece.Options(Game.Chess.ChessRules,Game.Chess.ChessPosition[,],Game.Chess.ChessPosition)~System.String[]")]

