﻿using System;
using System.Globalization;

namespace Game
{
    /// <summary>
    /// Class that describes a single position on a two dimensional game board.
    /// Columns are identified from left to right using charecters: a, b, c, d, etc. MaxCol indicates last column. 
    /// Rows are identified from bottom to top using charecters: 1, 2, 3, 4, etc. MaxRow indicates last row.
    /// </summary>
    public class Location
    {
        /// <summary>
        /// Column on the board as charecter.
        /// </summary>
        private char _Column;

        /// <summary>
        /// Constructor for creating an instance of Game.Location.
        /// </summary>
        /// <param name="column">Charecter representing location column.</param>
        /// <param name="row">Charecter representing location row.</param>
        public Location(char column, char row)
        {
            if(column < 'a')
            {
#pragma warning disable CA1303 // Do not pass literals as localized parameters
                throw new ArgumentException("must be >= a", nameof(column));
#pragma warning restore CA1303 // Do not pass literals as localized parameters
            }

            if(row < '1')
            {
#pragma warning disable CA1303 // Do not pass literals as localized parameters
                throw new ArgumentException($@"must be >= char '1'", nameof(row));
#pragma warning restore CA1303 // Do not pass literals as localized parameters
            }

            Column = column;
            Row = row;
        }

        /// <summary>
        /// Constructor for creating an instance of Game.Location by parsing a well formatted location string.
        /// </summary>
        /// <param name="boardLocation">Two charecter string describing the row and column of a position on the board.</param>
        /// <remarks>
        /// A well formated location string consists of two letters describing a column and row.
        /// For example, a piece in column f row 1 string chould be "f1".
        /// Note: Case internally represented as lower case.
        /// </remarks>
        public Location(string boardLocation)
        {
            if (string.IsNullOrEmpty(boardLocation) || boardLocation.Length != 2)
                throw new ArgumentException(boardLocation);

            Column = boardLocation[0];
            Row = boardLocation[1];
        }

        /// <summary>
        /// Max number of allowable rows.
        /// </summary>
        public int MaxRow { get; set; } = 8;

        /// <summary>
        /// Max number of allowable columns.
        /// </summary>
        public int MaxCol { get; set; } = 8;


        /// <summary>
        /// Lower case charecter representing location column.
        /// </summary>
        public char Column
        {
            get => _Column;
            private set => _Column = char.ToLower(value, CultureInfo.CreateSpecificCulture("en-US"));
        }

        /// <summary>
        /// Number charecter representing row.
        /// </summary>
        public char Row { get; private set; }

        /// <summary>
        /// Returns row array index associated with current row charecter.
        /// </summary>
        public int RowIndex
        {
            get {
                int tRow = (int)char.GetNumericValue(Row) - 1;

                if(tRow >= MaxRow)
                {
#pragma warning disable CA1065 // Do not raise exceptions in unexpected locations
                    throw new ArgumentOutOfRangeException($@"RowIndex {tRow} exceeds MaxRow {MaxRow}");
#pragma warning restore CA1065 // Do not raise exceptions in unexpected locations
                }

                return tRow;
            }

            set
            {
                var tRow = (int) char.GetNumericValue(Row) - 1;

                if (tRow != value && value >= 0 && value < MaxRow)
                {
                    var t = '1' + value;
                    Row = (char) t;
                }
            }
        }

        /// <summary>
        /// Returns column array index associated with current column charecter.
        /// </summary>
        public int ColumnIndex
        {
            get {
                int tCol = (int)Column - 'a';

                if(tCol >= MaxCol)
                {
#pragma warning disable CA1065 // Do not raise exceptions in unexpected locations
                    throw new ArgumentOutOfRangeException($@"RowIndex {tCol} exceeds MaxRow {MaxRow}");
#pragma warning restore CA1065 // Do not raise exceptions in unexpected locations
                }

                return tCol;
            }

            set
            {
                var tCol = Column - 'a';

                if (tCol != value && value >= 0 && value < MaxCol)
                {
                    var t = 'a' + value;
                    Column = (char) t;
                }
            }
        }

        /// <summary>
        /// Verifies current class properties are well defined.
        /// </summary>
        /// <returns>True when location column and row are valid.</returns>
        public bool IsValid()
        {
            return char.IsLetter(Column) &&
                   char.IsNumber(Row);
        }

        /// <summary>
        /// Provides string describing this positions column and row.
        /// For example, a piece in column f row 1 string will be "f1".
        /// </summary>
        /// <returns>String formatted ColumnRow.</returns>
        public override string ToString()
        {
            return $@"{Column}{Row}";
        }

        /// <summary>
        /// Overridden equals operator for comparing Game.Location objects.
        /// </summary>
        /// <param name="obj1">Left object to compare.</param>
        /// <param name="obj2">Right object to compare.</param>
        /// <returns>True when Game.Location objects are the equivalent.</returns>
        public static bool operator ==(Location obj1, Location obj2)
        {
            if (Equals(obj1, null))
            {
                if (Equals(obj2, null)) return true;

                return false;
            }

            return obj1.Equals(obj2);
        }

        /// <summary>
        /// Overridden not equals operator for comparing Game.Location objects.
        /// </summary>
        /// <param name="obj1">Left object to compare.</param>
        /// <param name="obj2">Right object to compare.</param>
        /// <returns>True when Game.Location objects are not equivalent.</returns>
        public static bool operator !=(Location obj1, Location obj2)
        {
            return !(obj1 == obj2);
        }

        /// <summary>
        /// Overridden equals method for determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">Object  object to compare.</param>
        /// <returns>True when specified Game.Location is equivalent to the current object.</returns>
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType()) return false;

            var piece = obj as Location;
            return piece != null &&
                   Column == piece.Column &&
                   Row == piece.Row;
        }

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            return HashCode.Combine(Column, Row);
        }
    }
}