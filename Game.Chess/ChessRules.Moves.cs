﻿

using System;
/**
* Static analysis notes:
*
* Class name must match file name
*/
namespace Game.Chess
{
    public partial class ChessRules
    {
        /// <summary>
        /// Enumeration describing all valid chess movement directions.
        /// </summary>
        [Flags]
        public enum Moves
        {
            LeftUpDiagonal,
            Up,
            RightUpDiagonal,
            Right,
            RightDownDiagonal,
            Down,
            LeftDownDiagonal,
            Left
        }
    }
}